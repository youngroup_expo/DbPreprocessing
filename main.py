import csv
import dataset
import random
import string
import sys


class DBPreprocess:

    def __init__(self, db_name, csv_path=None, anonymous_columns={}):
        self.db = dataset.connect('sqlite:///%s.db' % db_name)
        self.anonymous_columns = anonymous_columns
        if csv_path != None:
            with open(csv_path) as csvfile:
                col_names = [
                    'app_id',
                    'umi_id',
                    'req_id',
                    'fico_id',
                    'app_status',
                    'req_status',
                    'reason_type'
                ]
                self.reader_list = list(csv.DictReader(
                    csvfile, fieldnames=col_names, delimiter=';'))
        else:
            self.reader_list = None

    def clear_db(self):
        # for table_name in self.db.tables:
        #     self._drop_if_insignificant(self.db[table_name])

        if self.reader_list != None:
            self._preprocess_applications_with_csv()

    def anonymize(self):
        for table_name, col_names in self.anonymous_columns.iteritems():
            table = self.db[table_name]
            replaced_values = {}
            self.db.begin()
            for row in table.all():
                data = {'id': row['id']}
                for col_name in col_names:
                    if row[col_name] == None:
                        continue
                    if row[col_name] in replaced_values:
                        data.update({col_name: replaced_values[row[col_name]]})
                    else:
                        random_value = ''.join(random.choice(
                            string.ascii_uppercase + string.digits) for _ in range(len(row[col_name])))
                        replaced_values.update({row[col_name]: random_value})
                        data.update({col_name: random_value})
                table.update(data, ['id'])
            self.db.commit()

    def _preprocess_applications_with_csv(self):
        table = self.db['Application']
        ids_to_delete = []
        self.db.begin()
        for t_row in table.all():
            delete = True
            for r_row in self.reader_list:
                if t_row['FicoID'] == int(r_row['fico_id']):
                    data = {
                        'id': t_row['id'],
                        'AppStatus': r_row['app_status']
                    }
                    table.update(data, ['id'])
                    self.reader_list.remove(r_row)
                    delete = False
                    break

            if delete:
                ids_to_delete.append(t_row['id'])
        self.db.commit()

        self._delete_from_table(table, ids_to_delete)
        self._clear_no_parents()

    def _delete_from_table(self, table, ids_to_delete):
        ids_len = len(ids_to_delete)
        op_num = 1000

        for i in range(0, ids_len / op_num + 1):
            transaction_ids = ids_to_delete[i * op_num: i * op_num + op_num]
            self.db.begin()
            for id_to_delete in transaction_ids:
                table.delete(id=id_to_delete)
            self.db.commit()

    def _clear_no_parents(self, parent_table_name='Application'):
        dep_dict = self._create_table_dep_dict()

        for table_name in dep_dict[parent_table_name]:
            self._delete_rows_with_no_parent(table_name, parent_table_name)
            self._clear_no_parents(table_name)

    def _create_table_dep_dict(self):
        dep_dict = {}
        for table_name in self.db.tables:
            dep_dict[table_name] = []

        for table_name in self.db.tables:
            table = self.db[table_name]
            for col_name in table.columns:
                if col_name != 'old_id' and col_name.endswith('_id'):
                    dep_dict[col_name[:-3]].append(table_name)

        return dep_dict

    def _delete_rows_with_no_parent(self, table_name, parent_table_name):
        table = self.db[table_name]
        parent_table = self.db[parent_table_name]
        par_id_name = parent_table_name + '_id'

        ids_to_delete = []
        query = 'DELETE FROM $table WHERE id IN '\
            '(SELECT $table.id FROM $table ' \
            'LEFT JOIN $parent_table ' \
            'ON $table.$parent_table_id=$parent_table.id '\
            'WHERE $parent_table.id IS NULL)'
        query = query.replace('$table', table_name)
        query = query.replace('$parent_table', parent_table_name)

        self.db.query(query)
        # for row in table.all():
        #     if parent_table.find_one(id=row[par_id_name]) == None:
        #         ids_to_delete.append(row['id'])

        self._delete_from_table(table, ids_to_delete)

    """
    Drop table and return True if there are only primary and foreign 
    keys columns in it.
    Otherwise return False.
    """

    def _drop_if_insignificant(self, table):
        insignificant = True
        for col in table.columns:
            if col.endswith('id'):
                continue
            insignificant = False
            break

        if insignificant:
            table.drop()
            return True

        return False


def main(argv):
    anonymous_columns = {
        'Address': ['PostalCode'],
        'Applicant': ['AvgIncomeInRegion', 'DateOfBirth'],
        'Document': ['DocDate', 'DocSeries'],
        'Telephone': ['TelephoneNumber']
    }
    db_preprocess = DBPreprocess(argv[1], argv[2], anonymous_columns)
    db_preprocess.clear_db()
    db_preprocess.anonymize()

if __name__ == '__main__':
    main(sys.argv)
